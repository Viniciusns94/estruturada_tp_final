/********************************************************  
Trabalho Final: Estruturada.
	Professor: 
		Vinicius Ferreira da Silva

	Dupla: 	
		Guilherme Viera 				2014.1.08.012
		Vinicius Nogueira da Silva		2013.1.08.038
********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int passos=0;

void leituraDoMapa(int *col, int *lin, FILE **arq){
	*arq = fopen("labirinto.txt", "r");
	
	if(!*arq)	
		printf("ERRO! Arquivo não encontrado");
	else
		fscanf(*arq,"%d %d\n", &*col, &*lin);		
}

void prenderDragao(int linhas, int colunas, char mapa[][linhas], int *existePerigo){
	int i, j;
	for(i=0;i<colunas;i++){			
		for(j=0;j<linhas;j++){	
			
			if(mapa[i][j]=='D'){
				if(mapa[i][j-1]=='.') mapa[i][j-1]='#';
					
				if(mapa[i][j+1]=='.') mapa[i][j+1]='#';
				
				if(mapa[i-1][j]=='.') mapa[i-1][j]='#';
				
				if(mapa[i+1][j]=='.') mapa[i+1][j]='#';

				if(mapa[i][j-1]=='$') *existePerigo = 1;
					
				if(mapa[i][j+1]=='$') *existePerigo = 1;
				
				if(mapa[i-1][j]=='$') *existePerigo = 1;
				
				if(mapa[i+1][j]=='$') *existePerigo = 1;		
			}	
		}
	}
}

void posicaoNoMapa(int cont, int i, int j, int heroi[][2]){
	switch(cont){
		case 1:
			heroi[0][0] = i;
			heroi[0][1] = j;
			break;
		case 2:
			heroi[1][0] = i;
			heroi[1][1] = j;
			break;
		case 3:
			heroi[2][0] = i;
			heroi[2][1] = j;
			break;
		case 4:
			heroi[3][0] = i;
			heroi[3][1] = j;
			break;			
	}
}

void printarMapa(int lin, int col, char mapa[][lin], int heroi[][2], int *t, int *achouT){
	int i, j, yT, xT, cont=0, achouTesouro=0;
	printf("\n");

	for(i=0;i<col;i++){
		for(j=0;j<lin;j++){
			printf("%c", mapa[i][j]);
			if(mapa[i][j]=='@'){
				cont++;
				posicaoNoMapa(cont, i, j, heroi);
			}		 
		}
		printf("\n");			
	}
	if(mapa[t[0]][t[1]] != '$') *achouT = 1;	
}

void acharDistancia(int heroi[][2], int *t, float coordenadaOrdenada[][2]){
	int i, j, menor; float backup[2];
	
  	coordenadaOrdenada[0][0] = sqrt(pow((t[0]-heroi[0][0]),2) + pow((t[1]-heroi[0][1]),2));//ditância @1 até o herói
   	coordenadaOrdenada[0][1] = 1.0;//posição do heroi
    
  	coordenadaOrdenada[1][0] = sqrt(pow((t[0]-heroi[1][0]),2) + pow((t[1]-heroi[1][1]),2));//ditância @2 até o herói    
   	coordenadaOrdenada[1][1] = 2.0;
    
   	coordenadaOrdenada[2][0] = sqrt(pow((t[0]-heroi[2][0]),2) + pow((t[1]-heroi[2][1]),2));//ditância @3 até o herói
   	coordenadaOrdenada[2][1] = 3.0;
    	
   	coordenadaOrdenada[3][0] = sqrt(pow((t[0]-heroi[3][0]),2) + pow((t[1]-heroi[3][1]),2));//ditância @4 até o herói
   	coordenadaOrdenada[3][1] = 4.0;

   	for(i=0;i<3;i++){
		for(j=i+1;j<4;j++){
			if(coordenadaOrdenada[i][0] > coordenadaOrdenada[j][0]){ //j == 1: Se d1 > d2  ,  j==2: d2 > d3
					
				backup[0] = coordenadaOrdenada[j][0]; //d2   , d3
				backup[1] = coordenadaOrdenada[j][1]; //posição @2, posição @3 

				coordenadaOrdenada[j][0] = coordenadaOrdenada[i][0]; //d2 = d1    , d3 = d2			
    			coordenadaOrdenada[j][1] = coordenadaOrdenada[i][1];//posição @2 = posição @1 ,  posição @3 = posição @2

    			coordenadaOrdenada[i][0] = backup[0]; // d1 = d2   , d2 = d3
    			coordenadaOrdenada[i][1] = backup[1];// posição @1 = posição @2,  posição @2 = posição @3
    		}
    	}
	}
}

int passoParaEsquerda(int lin, int col, char mapa[][lin]){
	int i, j, verdade=0, andou=0; 

	for(i=0;i<col && !verdade;i++){
		for(j=0;j<lin && !verdade;j++){

			if(mapa[i][j]=='@'){
				if((mapa[i][j-1] !='#' && mapa[i][j-1] !='*') && (mapa[i+1][j-1] !='#' && mapa[i+1][j-1] !='*') && (j-1)>=0){
					
					mapa[i][j-1] = '@';
					mapa[i+1][j-1] = '@';

					mapa[i][j+1] = '*';
					mapa[i+1][j+1] = '*';
					andou++;
					passos++;
				}
				verdade = 1;		
			}	
		}
	}
	return andou;
}

int passoParaDireita(int lin, int col, char mapa[][lin]){
	int i, j, verdade=0, andou=0; 

	for(i=0;i<col && !verdade;i++){
		for(j=0;j<lin && !verdade;j++){

			if(mapa[i][j]=='@'){
				if((mapa[i][j+2] !='#' && mapa[i][j+2] !='*') && (mapa[i+1][j+2] !='#' && mapa[i+1][j+2] !='*') && (j+2)<lin){
					
					mapa[i][j+2] = '@';
					mapa[i+1][j+2] = '@';

					mapa[i][j] = '*';
					mapa[i+1][j] = '*';
					andou++;
					passos++;
				}
				verdade = 1;		
			}	
		}
	}
	return andou;
}

int passoParaCima(int lin, int col, char mapa[][lin]){
	int i, j, verdade=0, andou=0; 

	for(i=0;i<col && !verdade;i++){
		for(j=0;j<lin && !verdade;j++){
			
			if(mapa[i][j]=='@'){
				if((mapa[i-1][j] !='#' && mapa[i-1][j] !='*') && (mapa[i-1][j+1] !='#' && mapa[i-1][j+1] !='*') && (i-1)>=0){

					mapa[i-1][j] = '@';
					mapa[i-1][j+1] = '@';

					mapa[i+1][j] = '*';
					mapa[i+1][j+1] = '*';
					andou++;
					passos++;
				}
				verdade = 1;		
			}	
		}
	}
	return andou;
}

int passoParaBaixo(int lin, int col, char mapa[][lin]){
	int i, j, verdade=0, andou=0; 

	for(i=0;i<col && !verdade;i++){
		for(j=0;j<lin && !verdade;j++){

			if(mapa[i][j] == '@'){
				if((mapa[i+2][j] !='#' && mapa[i+2][j] !='*') && (mapa[i+2][j+1] !='#' && mapa[i+2][j+1] !='*') && (i+2)<=col){
					
					mapa[i+2][j] = '@';
					mapa[i+2][j+1] = '@';
					
					mapa[i][j] = '*';
					mapa[i][j+1] = '*';
					andou++;
					passos++;
				}
				verdade = 1;			
			}	
		}
	}
	return andou;
}

void analisaDistancia(int i, int j, int *t, float *distancia){
	*distancia = sqrt(pow((t[0]-i),2) + pow((t[1]-j),2));
}

void direcionar(int lin, int col, char mapa[][lin], int *t, int heroi[][2], int *achouT, int *v, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int dirCB, int dirDE){
	int menorDist, peso=0, heuDE=0, heuCB=0;
	float DIRouESQ, CimaouBaixo, distDIRouESQ, distCimaouBaixo;
	
	if((mapa[x1][y1] != '#' && mapa[x1][y1] != '*')&&(mapa[x2][y2] != '#' && mapa[x2][y2] != '*')){
		analisaDistancia(x1, y1, t, &distDIRouESQ);//lados
		peso=1;
								
		DIRouESQ = 1;
	}
	else peso = 700;

	heuDE = peso + distDIRouESQ;

	if((mapa[x3][y3] != '#' && mapa[x4][y4] != '*')&&(mapa[x4][y4] != '#' && mapa[x4][y4] != '*')){
		analisaDistancia(x3, y3, t, &distCimaouBaixo);//cimabaixo
		peso = 1;

		CimaouBaixo = 1;
	}
	else peso = 700;

	heuCB = peso + distCimaouBaixo;

	if(DIRouESQ && CimaouBaixo && !*achouT){

		if(heuDE >= heuCB) *v = moverHeroi(lin, col, mapa, dirCB, heroi, t, &*achouT);
		else *v = moverHeroi(lin, col, mapa, dirDE, heroi, t, &*achouT);  
	}
	if(DIRouESQ && !*v && !*achouT) *v = moverHeroi(lin, col, mapa, dirCB, heroi, t, &*achouT);									
														
	else if(CimaouBaixo && !*v && !*achouT) *v = moverHeroi(lin, col, mapa, dirDE, heroi, t, &*achouT);

	else if(!DIRouESQ && !CimaouBaixo) *v=0;
}

void aproximar(int lin, int col, char mapa[][lin], float part_proxima[][2], int *t, int heroi[][2], int *achouTesouro, int i){
	int aux, x, y, cont=0, verdade=0;
			
	aux = part_proxima[i][1];
	switch(aux){	
		case 1: //part_proxima é @1
			for(x=0;x<col;x++){				
				for(y=0;y<lin;y++){				
					if(mapa[x][y] == '@'){
						cont++;													
						if(cont == part_proxima[i][1]){
							direcionar(lin, col, mapa, t, heroi, &*achouTesouro, &verdade, x, y-1, x+1, y-1, x-1, y, x-1, y+1, 1, 4);

							if(!verdade && !*achouTesouro){ 
								aproximar(lin, col, mapa, part_proxima, t, heroi, &*achouTesouro, 1);
							} 
							if(verdade && !*achouTesouro){
								acharDistancia(heroi, t, part_proxima);
								aproximar(lin, col, mapa, part_proxima, t, heroi, &*achouTesouro, 0);
							}
						}
					}
				}
			}	
		break;	
		case 2:	//part_proxima é @2
			for(x=0;x<col;x++){			
				for(y=0;y<lin;y++){					
					if(mapa[x][y] == '@'){
						cont++;
						if(cont == part_proxima[i][1]){
							direcionar(lin, col, mapa, t, heroi, &*achouTesouro, &verdade, x, y+1, x+1, y+1, x-1, y, x-1, y-1, 1, 3);

							if(verdade && !*achouTesouro){
								aproximar(lin, col, mapa, part_proxima, t, heroi, &*achouTesouro, 0);
							}
							if(verdade && !*achouTesouro){
								acharDistancia(heroi, t, part_proxima);
								aproximar(lin, col, mapa, part_proxima, t, heroi, &*achouTesouro, 0);
							}
						}
					}
				}
			}
		break;
		case 3:	//part_proxima é @3
			for(x=0;x<col;x++){			
				for(y=0;y<lin;y++){					
					if(mapa[x][y] == '@'){
						cont++;
						if(cont == part_proxima[i][1]){
							direcionar(lin, col, mapa, t, heroi, &*achouTesouro, &verdade, x, y-1, x-1, y-1, x+1, y, x+1, y+1, 2, 4);

							if(!verdade && !*achouTesouro){
								i++; 
								aproximar(lin, col, mapa, part_proxima, t, heroi, &*achouTesouro, 3);
							}
							if(verdade && !*achouTesouro){
								acharDistancia(heroi, t, part_proxima);
								aproximar(lin, col, mapa, part_proxima, t, heroi, &*achouTesouro, 0);
							}
						}
					}
				}
			}
		break;
		case 4:	//part_proxima é @4
			for(x=0;x<col;x++){			
				for(y=0;y<lin;y++){					
					if(mapa[x][y] == '@'){
						cont++;
						if(cont == part_proxima[i][1]){
							direcionar(lin, col, mapa, t, heroi, &*achouTesouro, &verdade, x, y+1, x-1, y+1, x+1, y, x+1, y-1, 2, 3);

							if(!verdade && !*achouTesouro){
								aproximar(lin, col, mapa, part_proxima, t, heroi, &*achouTesouro, 2);
							}	
							if(verdade && !*achouTesouro){
								acharDistancia(heroi, t, part_proxima);
								aproximar(lin, col, mapa, part_proxima, t, heroi, &*achouTesouro, 0);
							}
						}
					}
				}
			}
		break;
	}								
}	

int moverHeroi(int lin, int col, char mapa[][lin], int direcao, int heroi[][2], int *t, int *achouT){
	int andou;
	switch(direcao){
		case 1:
			andou = passoParaCima(lin, col, mapa);
			if(andou)
				printarMapa(lin, col, mapa, heroi, t, &*achouT);
			return andou;
		case 2:
			andou = passoParaBaixo(lin, col, mapa);
			if(andou)
				printarMapa(lin, col, mapa, heroi, t, &*achouT);
			return andou;
		case 3:
			andou = passoParaDireita(lin, col, mapa);
			if(andou)
				printarMapa(lin, col, mapa, heroi, t, &*achouT);
			return andou;
		case 4:
			andou = passoParaEsquerda(lin, col, mapa);
			if(andou)
				printarMapa(lin, col, mapa, heroi, t, &*achouT);
			return andou;
	}
}

int main(void){
	int colunas, linhas, i, j; 
	FILE *arquivo;
	int t[2], heroi[4][2], part_proxima, achouTesouro, existeT=0, existePerigo=0;
	float distanPosi[4][2];
		
	leituraDoMapa(&colunas, &linhas, &arquivo);

	char mapa[colunas][linhas];
			
	for(i=0;i<colunas;i++){			
		for(j=0;j<linhas;j++){
			fscanf(arquivo,"%c\n", &mapa[i][j]);		
			if(mapa[i][j]=='$'){
				t[0] = i;
				t[1] = j;
				existeT=1;		
			}
		}			
	}				
	fclose(arquivo);
	
	prenderDragao(linhas, colunas, mapa, &existePerigo);
	
	printarMapa(linhas, colunas, mapa, heroi, t, &achouTesouro);
	
	if(!existePerigo){
		acharDistancia(heroi, t, distanPosi);	
	
		aproximar(linhas, colunas, mapa, distanPosi, t, heroi, &achouTesouro, 0);
	
		if(achouTesouro && existeT) printf("Herói encontrou o tesouro, andando %d passos.\n", passos);
	
		else puts("\nHerói não encontrou o tesouro");
	}
	else 
		puts("\nHerói não é bobo pra pegar o tesouro ao lado do dragão");
	return 0;
}	
